This project, its source code and build artifacts are licensed under
the [Tigra MIT License][license]. Please click the link for the full license text.

This basically says that, while Tigra Astronomy retains full rights to
this software, anyone may do anything at all with it, for free and without restriction.

In return for this freedom of access, you agree that whatever happens as a result,
the author cannot be held liable.

This license applies to material that is the original work of Tigra Astronomy.
The project uses third party libraries that may be covered by different licenses and
you should check the individual licensing terms of each library.

[license]: http://tigra.mit-license.org "Tigra Astronomy and Tigra Networks open source license"