//
//
//

#include "Json.h"
#include "AbstractResponse.h"

namespace Alpaca
	{
	ResponseBase::ResponseBase(bool prettyFormatter) : usePrettyFormat(prettyFormatter) {}

	std::string ResponseBase::getResponseString()
		{
		DynamicJsonDocument json(getMemoryPoolSize());
		populateJson(json.to<JsonObject>());
		std::ostringstream out;
		if (usePrettyFormat)
			serializeJsonPretty(json, out);
		else
			serializeJson(json, out);
		return out.str();
		}

	int ResponseBase::getMemoryPoolSize()
		{
		return 83;
		}

	void ResponseBase::populateJson( JsonObject json)
		{
		json["errorNumber"] = errorNumber;
		json["errorMessage"] = errorDescription;
		}


	int BooleanResponse::getMemoryPoolSize()
		{
		return 22 + ResponseBase::getMemoryPoolSize();
		}

	void BooleanResponse::populateJson(JsonObject json)
		{
		json["Value"] = value;
		ResponseBase::populateJson(json);
		}

	int EchoResponse::getMemoryPoolSize()
		{
		return 128 + ResponseBase::getMemoryPoolSize();
		}

	void EchoResponse::populateJson(JsonObject json)
		{
		json["echo"] = value;
		}




	}


