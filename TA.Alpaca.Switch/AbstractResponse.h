// AlpacaResponse.h

#pragma once

#include <string>
#include <sstream>
#include "Json.h"
#include "Alpaca.h"

namespace Alpaca
	{
	class ResponseBase
		{
		public:
			explicit ResponseBase(bool prettyFormatter = false);
			std::string getResponseString();
		private:
			bool usePrettyFormat;
			ErrorId errorNumber;
			ErrorDescription errorDescription;
		protected:
			virtual int getMemoryPoolSize();
			virtual void populateJson(JsonObject json);
		};

	class BooleanResponse : public ResponseBase
		{
		int getMemoryPoolSize() override;
		void populateJson(JsonObject json) override;
		bool value;
		};

	class EchoResponse : public ResponseBase
		{
		int getMemoryPoolSize() override;
		void populateJson(JsonObject json) override;
		std::string value;
	public:
		EchoResponse(std::string message) : value(message) {  }
		};
	}

