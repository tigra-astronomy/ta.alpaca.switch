#pragma once
#include <string>
#include <Endpoints.h>
#include "AbstractResponse.h"

// derive our own Request object from a Rest::UriRequest (that contains resolved Url stuff like url args)
class RestRequest : public Rest::UriRequest
	{
public:
	// we could use ArduinoJson objects here instead, these are whatever variables we want our handlers to have access to.
	// The core Restfully API doesnt deal with request or response data (Restfully Platform does) so we'll have to parse
	// Json content or whatever text ourselves.
	std::string request;
	Alpaca::ResponseBase response;

	void setResponse(Alpaca::ResponseBase response) { this->response = response; }

	// we derive from UriRequest, and when creating our Request object we'll copy stuff from the resolved Request we're
	// given into our base object.
	explicit RestRequest(const Rest::UriRequest& rr)
		: Rest::UriRequest(rr) { }
	};

// declare what our Endpoint handler functions will look like (typedef)
using RequestHandler = short(*)(RestRequest&);

// now declare Endpoints collection that will hold URIs and handlers of our own prototype (typedef)
using Endpoints = Rest::Endpoints<RequestHandler>;

using HttpResult = short;

constexpr HttpResult HttpOk = 200;
constexpr HttpResult HttpServerError = 500;
constexpr HttpResult HttpNotFound = 404;