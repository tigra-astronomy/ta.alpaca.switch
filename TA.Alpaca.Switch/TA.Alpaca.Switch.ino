/*
 Name:		TA.ino
 Created:	8/29/2019 10:46:59 PM
 Author:	Tim
*/

#include <sstream>
#include <WiFiNINA.h>
#include <WiFiServer.h>
#include <WiFiUdp.h>
#include <Endpoints.h>
#include <ArduinoHttpServer.h>
#include "Json.h"
#include "AbstractResponse.h"
#include "AscomSwitch.h"
#include "Version.h"
#include "Secrets.h"
#include "RestRequest.h"
#include "Alpaca.h"

Endpoints endpoints; // Rest endpoints/routes
WiFiUDP Udp;
WiFiServer server = WiFiServer(Alpaca::defaultApiPort);
char packetBuffer[255]; //buffer to hold incoming HTTP or UDP packet


short loop_handleRequest(Rest::HttpMethod method, const char* requestUrl)
	{
	// try to resolve the URI to an Endpoint handler
	Endpoints::Request endpoint = endpoints.resolve(method, requestUrl);
	if (endpoint)
		{
		// tests if ep.status==Rest::UriMatched
		// we resolved the Url to an Endpoint
		// we now have an Endpoint handler function, and any arguments we got in the Url

		// declare a new request object (our object) that contains request/response text
		RestRequest request(endpoint); // copies stuff from resolved endpoint into our request (URI args for example)

		// now execute the handler giving it our Request object
		// todo: we should parse Request POST data if exists, such as parsing Json object
		int rs = endpoint.handler(request);

		// todo: now our req should have response data, we can output to http client
		Serial.println(request.response.getResponseString().c_str());

		// you many want to look at ArduinoPlatform.h around line 108 to 165 for more examples on handling request/response data and errors

		return 200; // or whatever
		}
	else
		{
		Serial.print("failed to resolve ");
		Serial.println(requestUrl);
		}
	return HttpNotFound;
	}


void CheckForHttpRequest()
	{
	auto wifiClient = server.available();
	auto httpRequest = ArduinoHttpServer::StreamHttpRequest<512>(wifiClient);
	bool success = httpRequest.readRequest();
	if (success)
		{
		Serial.println("HTTP request");
		auto requestBody = httpRequest.getBody();
		auto requestContentLength = httpRequest.getContentLength();
		auto requestContentType = httpRequest.getContentType();
		auto requestUri = httpRequest.getResource().toString();
		char* uri = new char[128];
		requestUri.toCharArray(uri, 128);

		Serial.println(requestUri);
		Serial.println(requestBody);

		auto method = httpRequest.getMethod();
		switch (method)
			{
		case ArduinoHttpServer::MethodEnum::MethodGet:
			loop_handleRequest(Rest::HttpGet, uri /*, wifiClient */);
			break;

		default:
			break;
			}

		wifiClient.stop();
		}
	}

void CheckForDiscovery()
	{
	// if there's data available, read a packet
	int packetSize = Udp.parsePacket();
	if (packetSize)
		{
		Serial.print("Received packet of size: ");
		Serial.println(packetSize);
		Serial.print("From ");
		IPAddress remoteIp = Udp.remoteIP();
		Serial.print(remoteIp);
		Serial.print(", on port ");
		Serial.println(Udp.remotePort());

		// read the packet into packetBufffer
		int len = Udp.read(packetBuffer, 255);
		if (len > 0)
			{
			//Ensure that it is null terminated
			packetBuffer[len] = 0;
			}
		Serial.print("Contents: ");
		Serial.println(packetBuffer);

		// No oversized packets allowed
		if (len < 16)
			{
			return;
			}

		if (strncmp("alpaca discovery", packetBuffer, 16) != 0)
			{
			return;
			}

		std::ostringstream responseStream;
		responseStream << "alpaca here:" << Alpaca::defaultApiPort;
		std::string stdString = responseStream.str();
		const char* outCstr = stdString.c_str();
		auto outLength = stdString.length();
		// send a reply, to the IP address and port that sent us the packet we received
		Udp.beginPacket(Udp.remoteIP(), Udp.remotePort());
		Udp.write((const uint8_t*)outCstr, outLength);
		Udp.endPacket();
		}
	}

void printWifiStatus()
	{
	// print the SSID of the network you're attached to:
	Serial.print("SSID: ");
	Serial.println(WiFi.SSID());

	// print your board's IP address:
	IPAddress ip = WiFi.localIP();
	Serial.print("IP Address: ");
	Serial.println(ip);

	// print the received signal strength:
	long rssi = WiFi.RSSI();
	Serial.print("signal strength (RSSI):");
	Serial.print(rssi);
	Serial.println(" dBm");
	}


void connectWiFi()
	{
	// check for the WiFi module:
	if (WiFi.status() == WL_NO_MODULE)
		{
		Serial.println("Communication with WiFi module failed!");
		// don't continue
		while (true);
		}

	String fv = WiFi.firmwareVersion();
	if (fv < "1.0.0")
		{
		Serial.println("Please upgrade the firmware");
		}

	// attempt to connect to the Wifi network
	WiFi.setHostname("alpaca");
	WiFi.noLowPowerMode();
	WiFi.begin(Secret::ssid, Secret::encryptionKey);

	auto count = 0;
	while (WiFi.status() != WL_CONNECTED)
		{
		delay(1000);
		Serial.print(".");
		if (count>10)
			{
			WiFi.end();
			WiFi.begin(Secret::ssid, Secret::encryptionKey);
			}
		}

	Serial.println("Connected to wifi");
	printWifiStatus();

	Udp.begin(Alpaca::defaultDiscoveryPort);
	server.begin();

	Serial.println("Listening for requests...");
	}

// the setup function runs once when you press reset or power the board
void setup()
	{
	Serial.begin(115200);
	while (!Serial.availableForWrite());
	Serial.print("Semantic version ");
	Serial.println(SemanticVersion);

	// add some endpoints
	Serial.println("adding endpoints");
	endpoints
		.on("/api/v1/:deviceType(string)/:deviceNumber(number)")
		.GET("echo/:msg(string)", AscomSwitch::echo);

	connectWiFi();
	}

// the loop function runs over and over again until power down or reset
void loop()
	{
	CheckForDiscovery();
	CheckForHttpRequest();
	}
