#pragma once

namespace Secret
	{
	constexpr char ssid[] = "TN-AP1130AG";
	constexpr char encryptionKey[] = "GoneWithTheWind";
	constexpr int keyIndex = 0; // your network key Index number (needed only for WEP)
	}
