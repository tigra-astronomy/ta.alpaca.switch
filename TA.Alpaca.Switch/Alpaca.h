#pragma once
#include <string>
#include <cstdint>

namespace Alpaca
	{
	using Port = unsigned int;
	using HttpResult = short;
	constexpr Port defaultDiscoveryPort = 32227; //The Alpaca Discovery test port
	constexpr Port defaultApiPort = 4567;        //The (fake) port that the Alpaca API would be available on
	constexpr HttpResult Success = 200;
	constexpr HttpResult InvalidRequest = 400;
	constexpr HttpResult ServerError = 500;

	using ErrorId = unsigned int;
	using ErrorDescription = std::string;
	using TransactionId = unsigned int;
	}