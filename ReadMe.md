# ASCOM Alpaca Switch Driver Proof of Concept Implementation for Arduino

This project is an [ASCOM Alpaca][alpaca] driver that implements the ASCOM ISwitch interface.

The project targets the Arduino platform, and specifically the Arduino MKR 1010 WiFi.
This is a 32-bit ARM core that is capable of sustaining large, more sophisticated
projects and features a built-in Wi-Fi chipset, making it well-suited for an
ASCOM Alpaca implementation.

The project has been produced by [Tigra Astronomy][tigra] and is made available under the [MIT
open source license][license].

## Developer Notes

# Build

The project was developed in Visual Studio 2019 using the VisualMicro extension. Both products are available for free. VisualMicro claims to retain compatibility with the Arduino IDE, but we haven't tried that and we cannot gaurantee that the project is buildable in that environment. The Arduino IDE is a somewhat inadequate environment in any case, so we highly recomment using Visual Studio with the VisualMicro extension. You will wonder why you put up with the Arduino IDE for so long!

# Automatic Versioning

The code is automatically versioned based on Git commit history, using the GitVersion command line tools and a pre-build action that runs a PowerShell script to generate and overwrite the `Version.h` file during the build. The `Version.h` file in the project is a placeholder that helps with IntelliSense. We have produced [a video about this technique][versioning] and you are referred to that video for details.



[alpaca]: https://ascom-standards.org/Developer/Alpaca.htm "ASCOM cross-platform ReST interface"
[ascom]: https://ascom-standards.org "ASCOM Standards web site for documentation and downloads"
[tigra]: http://tigra-astronomy.com "Tigra Astronomy web site"
[license]: http://tigra.mit-license.org "Tigra Astronomy and Tigra Networks open source license"
[versioning]: https://youtu.be/P4B6PTP6aAk "Video: Automatic Versioning for Arduino code with GitVersion"